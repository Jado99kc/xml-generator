import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    feed:[]
  },
  mutations: {
    SET_ARTICLES(state, feed){
      state.feed = feed
    }
  },
  actions: {
    async getArticles({commit}){
      console.log('dentro')
      try {
        const response =  await fetch('https://sleepy-ravine-01441.herokuapp.com/articles',{
          method: 'GET',
          headers: {
            'Content-Type': 'application/json'
          }
        })
        const feed =  await response.json()
        commit('SET_ARTICLES', feed)
      } catch (error) {
        console.error(error)
      }
    }
  },
  modules: {
  }
})
